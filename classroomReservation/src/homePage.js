import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Button,
  Modal,
  Alert,
} from "react-native";
import api from "./axios/axios";
import DateTimePicket from "./components/datePicker";
import CheckDays from "./components/checkDays";
import { useNavigation } from "@react-navigation/native";

const HomePage = ({ route }) => {
  const { user, nameUser } = route.params; // Dados do Usuário após Login
  const [classrooms, setClassrooms] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [scheduleDefault, setScheduleDefault] = useState({
    dateStart: "",
    timeStart: "",
    dateEnd: "",
    timeEnd: "",
    days: [],
    user: user, // Esse deve pegar o User autenticado
    classroom: "", // Esse não precisa ser preenchida
  });
   const [schedule, setSchedule] = useState(scheduleDefault);
   const navigation = useNavigation();

  useEffect(() => {
    const fetchClassrooms = async () => {
      try {
        const response = await api.getAllClassroom();
        setClassrooms(response.data.classrooms);
      } catch (error) {
        console.error("Erro ao obter salas:", error);
      }
    };

    fetchClassrooms();
  }, []);

  const handleReservation = (classroomNumber)=>{
    //Atualizar numero da sala
    setSchedule({
      ...scheduleDefault,
      classroom:classroomNumber
    });
    //Abre modal
    setShowModal(true)
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{nameUser}</Text>
      <FlatList
        data={classrooms}
        keyExtractor={(item) => item.number.toString()}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text style={styles.itemTitle}>Sala: {item.number}</Text>
            <Text style={styles.itemText}>Descrição: {item.description}</Text>
            <Text style={styles.itemText}>Capacidade: {item.capacity}</Text>
            <View style={styles.buttonContainer}>
              <Button
                title="Reservar"
                onPress={() => handleReservation(item.number)}
                color="blue"
              />
              <Button
                title="Consultar"
                // onPress={() => handleConsult(item.number)}
                color="green"
              />
            </View>
          </View>
        )}
      />

      {/* Modal */}
      <Modal visible={showModal} animationType="slide"  transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer} >
            <Text style={styles.modalTitle}>Formulario de reserva</Text>
            <CheckDays selectedDays={schedule.days} setSchedule={setSchedule}></CheckDays>
            <View style={styles.buttonContainer}>
              <Button title={"Reservar " + schedule.classroom} color="green" onPress={() => setShowModal(false)} />
              <Button title="Cancelar" color="red" onPress={() => setShowModal(false)}/>
            </View>
          </View>
        </View>

      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 10,
    backgroundColor: "#084d6e",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
    color: "#fff",
  },
  item: {
    backgroundColor: "#fff",
    marginBottom: 20,
    padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  itemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  itemText: {
    fontSize: 16,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Define um fundo semi-transparente
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    backgroundColor: "#084d6e",
    borderRadius: 10,
    padding: 20,
    width: "80%", // Define a largura do modal
    maxHeight: "70%", // Define a altura máxima do modal
    justifyContent: "center",
    alignItems: "center",
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#fff",
  },
  input: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    marginBottom: 10,
    padding: 10,
    width: "100%",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
  },
  picker: {
    width: "100%",
    marginBottom: 10,
  },
});

export default HomePage;
